# hamster

### Build and run
```bash
meson setup build --prefix=/usr
cd build
ninja
./app/hamster
```

### Install
```bash
ninja install
```

### Run
```bash
hamster
```
