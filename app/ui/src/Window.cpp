#include "Window.h"

Window::Window() : label{"Welcome to hamster!"} {
    set_title("Hamster");
    set_default_size(600, 320);

    set_child(label);
}
