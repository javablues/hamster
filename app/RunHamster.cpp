#include <adwaita.h>
#include <gtkmm.h>
#include <ui/include/Window.h>

int main(int argc, char *argv[]) {
    auto app = Gtk::Application::create("org.javablues.hamster");
    adw_style_manager_set_color_scheme(adw_style_manager_get_default(), ADW_COLOR_SCHEME_PREFER_LIGHT);

    return app->make_window_and_run<Window>(argc, argv);
}
